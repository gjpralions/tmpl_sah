<div ng-controller="ProductsCtrl">
	<label>Search: <input ng-model="searchText"></label>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
			</tr>
		</thead>
		<tbody>
			<tr
				ng-repeat="post in products | filter:searchText | orderBy:ProductName">
				<td>{{post.productName}}</td>
			</tr>
		</tbody>
	</table>
</div>