<%@page import="gjp.core.models.AppConfig"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="tmpl_sah">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- JQUERY -->
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"></script>
<!-- ANGULAR JS -->
<script type="text/javascript"
	src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/angular-route.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/app.js" />"></script>
<!-- TWITTER BOOTSTRAP -->
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" />
<link href="<c:url value="/resources/css/bootstrap-theme.min.css"  />" />
<link href="<c:url value="/resources/css/font-awesome.css"  />" />


<title>${app.getAppName()}-${app.getAppVersion() }|
	${app.getAuthor() }</title>
</head>
<body>


	<span style="float: right"> <a href="?lang=en/">en</a> | <a
		href="?lang=de/">de</a>
	</span>
	<div class="container" ng-controller="ProductsCtrl">
		<h3>
			<spring:message code="label.title" />
		</h3>
		<div ng-view></div>
	</div>
	<!-- /container -->
</body>
</html>