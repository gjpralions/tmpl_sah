var tmpl_sah = angular.module('tmpl_sah', ['ngRoute']);
tmpl_sah.config(function($routeProvider) {
    $routeProvider
	    .when('/', {
	    	templateUrl : 'layouts/partials/products.jsp',
	        controller  : 'ProductsCtrl'
	    })
        ;
    
});
tmpl_sah.controller('ProductsCtrl',	[ '$scope', '$http', function($scope, $http){
	$http.get('/TMPL_SAH/products').success(function(data){
		$scope.products = data;
		console.log("DATA IS: "+JSON.stringify(data));
	});
}]);
