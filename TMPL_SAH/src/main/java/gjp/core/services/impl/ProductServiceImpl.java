package gjp.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gjp.core.dao.ProductDao;
import gjp.core.models.Product;
import gjp.core.services.ProductService;
@Component
public class ProductServiceImpl implements ProductService{

	 @Autowired
	 ProductDao productDao;  
	 
	@Override
	public int insertRow(Product product) {
		return productDao.insertRow(product);
	}

	@Override
	public List<Product> getList() {
		return productDao.getList();
	}

	@Override
	public Product getRowById(int id) {
		return productDao.getRowById(id);
	}

	@Override
	public int updateRow(Product product) {
		return productDao.updateRow(product);
	}

	@Override
	public int deleteRow(int id) {
		return productDao.deleteRow(id);
	}

}
