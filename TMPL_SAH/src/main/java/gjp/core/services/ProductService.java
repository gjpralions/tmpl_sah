package gjp.core.services;

import java.util.List;

import gjp.core.models.Product;

public interface ProductService {
	 public int insertRow(Product employee);  
	 public List<Product> getList();  
	 public Product getRowById(int id);  
	 public int updateRow(Product employee);  
	 public int deleteRow(int id); 
}
