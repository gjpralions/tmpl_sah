package gjp.core.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import gjp.core.dao.ProductDao;
import gjp.core.models.Product;

@Component
public class ProductDaoImpl implements ProductDao {
	@Autowired  SessionFactory sessionFactory;
	public ProductDaoImpl() {
		// TODO Auto-generated constructor stub
	}
	public ProductDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public int insertRow(Product product) {
		getSession().merge(product);
		Serializable id = getSession().getIdentifier(product);
		return (Integer) id;
	}

	private Session getSession() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Product> getList() {
	    return (List<Product>)  sessionFactory.getCurrentSession().createQuery("from Product").list();
	}

	@Override
	public Product getRowById(int id) {
		return (Product) getSession().get(Product.class, id);
	}

	@Override
	public int updateRow(Product product) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(product);
		tx.commit();
		Serializable id = session.getIdentifier(product);
		session.close();
		return (Integer) id;	
	}

	@Override
	public int deleteRow(int id) {
		getSession().delete(getRowById(id));
		return 0;
	}

}
