package gjp.core.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import gjp.core.models.Product;
import gjp.core.services.ProductService;


@org.springframework.web.bind.annotation.RestController
public class RestController {
	@Autowired private ProductService productsDao;
	
	@RequestMapping(value ="/products", method = RequestMethod.GET)
	public @ResponseBody List<Product> getProductsList(Model model) {
		System.out.println("in products");
		return productsDao.getList();
	}
}
