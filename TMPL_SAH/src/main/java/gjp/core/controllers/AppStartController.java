package gjp.core.controllers;

import java.util.List;
import java.util.logging.Logger;

import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import gjp.core.models.AppConfig;
import gjp.core.models.Product;
import gjp.core.services.ProductService;

@Controller
@RequestMapping("/")
public class AppStartController {
	String message = "Welcome to Spring MVC!";
	@RequestMapping("/")
	public ModelAndView index(Model model) {
		System.out.println("in controller");
		ModelAndView mv = new ModelAndView("/layouts/themes/default");
		mv.addObject("message", message);
		mv.addObject("app", new AppConfig());
		return mv;
	}
}
