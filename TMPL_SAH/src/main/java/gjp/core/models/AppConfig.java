package gjp.core.models;

import java.util.Calendar;
import java.util.Currency;
import java.util.Locale;

public class AppConfig {
	private String AppName, AppVersion, Author;
	private int CopyrightsYear;
	private Currency DefaultCurrency;
	private Locale DefaultLanguage;


	public AppConfig(String appName, String appVersion, String author, int copyrightsYear, Currency defaultCurrency,
			Locale defaultLanguage) {
		System.out.println("CONFIGURING.. ");
		AppName = appName;
		AppVersion = appVersion;
		Author = author;
		CopyrightsYear = copyrightsYear;
		DefaultCurrency = defaultCurrency;
		DefaultLanguage = defaultLanguage;
	}


	public AppConfig() {
		AppName = "MST SOLUTIONS";
		AppVersion = "0.1";
		Author = "Janani";
		CopyrightsYear =  Calendar.getInstance().get(Calendar.YEAR);
		DefaultCurrency = null;
		DefaultLanguage = null;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppConfig [AppName=" + AppName + ", AppVersion=" + AppVersion + ", Author=" + Author
				+ ", CopyrightsYear=" + CopyrightsYear + ", DefaultCurrency=" + DefaultCurrency + ", DefaultLanguage="
				+ DefaultLanguage + "]";
	}


	public String getAppName() {
		return AppName;
	}


	public void setAppName(String appName) {
		AppName = appName;
	}


	public String getAppVersion() {
		return AppVersion;
	}


	public void setAppVersion(String appVersion) {
		AppVersion = appVersion;
	}


	public String getAuthor() {
		return Author;
	}


	public void setAuthor(String author) {
		Author = author;
	}


	public int getCopyrightsYear() {
		return CopyrightsYear;
	}


	public void setCopyrightsYear(int copyrightsYear) {
		CopyrightsYear = copyrightsYear;
	}


	public Currency getDefaultCurrency() {
		return DefaultCurrency;
	}


	public void setDefaultCurrency(Currency defaultCurrency) {
		DefaultCurrency = defaultCurrency;
	}


	public Locale getDefaultLanguage() {
		return DefaultLanguage;
	}


	public void setDefaultLanguage(Locale defaultLanguage) {
		DefaultLanguage = defaultLanguage;
	}
	
	
	
}

