package gjp.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {

	public Product() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@GeneratedValue
	private int ProductID;

	private String ProductName, QuantityPerUnit;

	private int SupplierID, CategoryID, UnitsInStock, UnitsOnOrder, ReorderLevel;

	private double UnitPrice;

	private boolean Discontinued;

	public Product(int productID, String productName, String quantityPerUnit, int supplierID, int categoryID,
			int unitsInStock, int unitsOnOrder, int reorderLevel, double unitPrice, boolean discontinued) {
		super();
		ProductID = productID;
		ProductName = productName;
		QuantityPerUnit = quantityPerUnit;
		SupplierID = supplierID;
		CategoryID = categoryID;
		UnitsInStock = unitsInStock;
		UnitsOnOrder = unitsOnOrder;
		ReorderLevel = reorderLevel;
		UnitPrice = unitPrice;
		Discontinued = discontinued;
	}

	@Override
	public String toString() {
		return "Product [getProductID()=" + getProductID() + ", getProductName()=" + getProductName()
				+ ", getQuantityPerUnit()=" + getQuantityPerUnit() + ", getSupplierID()=" + getSupplierID()
				+ ", getCategoryID()=" + getCategoryID() + ", getUnitsInStock()=" + getUnitsInStock()
				+ ", getUnitsOnOrder()=" + getUnitsOnOrder() + ", getReorderLevel()=" + getReorderLevel()
				+ ", getUnitPrice()=" + getUnitPrice() + ", isDiscontinued()=" + isDiscontinued() + "]";
	}

	public int getProductID() {
		return ProductID;
	}

	public void setProductID(int productID) {
		ProductID = productID;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public String getQuantityPerUnit() {
		return QuantityPerUnit;
	}

	public void setQuantityPerUnit(String quantityPerUnit) {
		QuantityPerUnit = quantityPerUnit;
	}

	public int getSupplierID() {
		return SupplierID;
	}

	public void setSupplierID(int supplierID) {
		SupplierID = supplierID;
	}

	public int getCategoryID() {
		return CategoryID;
	}

	public void setCategoryID(int categoryID) {
		CategoryID = categoryID;
	}

	public int getUnitsInStock() {
		return UnitsInStock;
	}

	public void setUnitsInStock(int unitsInStock) {
		UnitsInStock = unitsInStock;
	}

	public int getUnitsOnOrder() {
		return UnitsOnOrder;
	}

	public void setUnitsOnOrder(int unitsOnOrder) {
		UnitsOnOrder = unitsOnOrder;
	}

	public int getReorderLevel() {
		return ReorderLevel;
	}

	public void setReorderLevel(int reorderLevel) {
		ReorderLevel = reorderLevel;
	}

	public double getUnitPrice() {
		return UnitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}

	public boolean isDiscontinued() {
		return Discontinued;
	}

	public void setDiscontinued(boolean discontinued) {
		Discontinued = discontinued;
	}

}
